:w     # for write or save
esc    # normal mode :)
hjkl   # for navigation left, down, up right
:q     # quit
i      # insert mode
:      # command mode
:3     # go to line 3
w      # go next word
b      # go back word
gg     # go to the 1st line
shft g # go to the last line
x      # delete character
o      # go to the next line in insert mode
u      # undo
ctrl r # redo


shft v    # select row
y         # copy or yank
esc p     # paste

dw        # delete current word
dd        # delte current line
shft $    # go to the last char in the line
shft ^    # go the the first char in the line

:wq       # write and quit
:q!       # quit forcefully

this is a part of me

:split    # split pane horizontally
:vsplit   # split pane vertically
:q        # to destroy pane

ctrl w <hjkl>  # move pane focus
